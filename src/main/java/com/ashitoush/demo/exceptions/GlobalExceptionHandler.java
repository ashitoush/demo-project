package com.ashitoush.demo.exceptions;

import com.ashitoush.demo.constants.MessageConstant;
import com.ashitoush.demo.dto.GlobalApiResponse;
import com.ashitoush.demo.util.CustomMessageSource;
import lombok.RequiredArgsConstructor;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private final CustomMessageSource customMessageSource;
    private GlobalApiResponse response;

    @ExceptionHandler(AppException.class)
    public ResponseEntity<GlobalApiResponse> appExceptionHandler(AppException e) {
        String message = e.getMessage();
        GlobalApiResponse response = GlobalApiResponse.builder()
                .status(false)
                .message(customMessageSource.get(MessageConstant.ERROR))
                .data(message)
                .build();
        return ResponseEntity.badRequest().body(response);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<?> dataIntegrityViolationHandler(DataIntegrityViolationException exception) {
        if (exception.getCause() instanceof ConstraintViolationException) {
            ConstraintViolationException constraintViolationException = (ConstraintViolationException) exception.getCause();
            String message = constraintViolationException.getConstraintName().replace("uk_", "").split("_")[1];
            response = GlobalApiResponse.builder()
                    .status(false)
                    .message(customMessageSource.get(MessageConstant.ERROR))
                    .data(customMessageSource.get(MessageConstant.UNIQUE_CONSTRAINT_ERROR, message))
                    .build();
        }
        return ResponseEntity.badRequest().body(response);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> message = new ArrayList<>();
        List<FieldError> fieldErrorList = exception.getFieldErrors();

        for (FieldError fieldError : fieldErrorList) {
            message.add(fieldError.getDefaultMessage());
        }
        response = GlobalApiResponse.builder()
                .status(false)
                .message(customMessageSource.get(MessageConstant.ERROR))
                .data(message)
                .build();
        return ResponseEntity.badRequest().body(response);
    }
}
