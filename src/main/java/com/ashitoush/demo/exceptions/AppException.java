package com.ashitoush.demo.exceptions;

public class AppException extends RuntimeException {
    public AppException(String message) {
        super(message);
    }
}
