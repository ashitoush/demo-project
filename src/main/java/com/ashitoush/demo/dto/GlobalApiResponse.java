package com.ashitoush.demo.dto;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GlobalApiResponse {
    private boolean status;
    private String message;
    private Object data;
}
