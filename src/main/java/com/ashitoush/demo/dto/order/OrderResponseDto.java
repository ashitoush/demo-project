package com.ashitoush.demo.dto.order;

import com.ashitoush.demo.dto.product.ProductResponseDto;
import com.ashitoush.demo.dto.user.UserResponseDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class OrderResponseDto {
    private Integer id;
    private LocalDate orderDate;
    private ProductResponseDto product;
    private BigDecimal amount;
    private UserResponseDto users;
}
