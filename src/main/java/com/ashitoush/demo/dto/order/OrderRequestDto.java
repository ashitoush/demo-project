package com.ashitoush.demo.dto.order;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class OrderRequestDto {
    private Integer id;
    @NotNull(message = "Order Date Cannot be Null")
    private LocalDate orderDate;
    @NotNull(message = "Product Id Cannot be Null")
    private Integer productId;
    @NotNull(message = "Amount cannot be null")
    private BigDecimal amount;
    @NotNull(message = "User Id Cannot be Null")
    private Integer userId;
}
