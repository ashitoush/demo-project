package com.ashitoush.demo.dto.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserRequestDto {
    private Integer id;
    @NotEmpty(message = "Full Name cannot be Empty")
    private String fullName;
    @NotEmpty(message = "Contact Number cannot be Empty")
    private String contactNo;
    @NotEmpty(message = "Address Cannot be Empty")
    private String address;
}
