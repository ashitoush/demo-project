package com.ashitoush.demo.dto.product;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequestDto {
    private Integer id;
    @NotEmpty(message = "Name Cannot be Empty")
    private String name;
    @NotEmpty(message = "Description Cannot be Empty")
    private String description;
    @NotNull(message = "Price Cannot be Null")
    private BigDecimal price;
    @NotNull(message = "Stock Quantity Cannot be Null")
    private Integer stockQuantity;
}
