package com.ashitoush.demo.entity;

import lombok.*;

import javax.persistence.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "users", uniqueConstraints = @UniqueConstraint(columnNames = "contact_no", name = "uk_users_contactnumber"))
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_seq_gen")
    @SequenceGenerator(name = "users_seq_gen", sequenceName = "users_seq", initialValue = 1, allocationSize = 1)
    private Short id;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @Column(name = "contact_no", nullable = false, length = 10)
    private String contactNo;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id", nullable = false, foreignKey = @ForeignKey(name = "fk_users_address"))
    private Address address;
}
