package com.ashitoush.demo.service;

import com.ashitoush.demo.dto.user.UserRequestDto;
import com.ashitoush.demo.dto.user.UserResponseDto;

import java.util.List;

public interface UserService {
    Integer saveUser(UserRequestDto requestDto);
    void updateUser(UserRequestDto requestDto);
    UserResponseDto getUserById(Integer id);
    List<UserResponseDto> getAllUsers();
    void deleteUser(Integer id);
}
