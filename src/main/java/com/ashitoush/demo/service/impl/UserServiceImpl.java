package com.ashitoush.demo.service.impl;

import com.ashitoush.demo.constants.FieldConstant;
import com.ashitoush.demo.constants.MessageConstant;
import com.ashitoush.demo.dto.user.UserRequestDto;
import com.ashitoush.demo.dto.user.UserResponseDto;
import com.ashitoush.demo.entity.Users;
import com.ashitoush.demo.exceptions.AppException;
import com.ashitoush.demo.mapper.UsersMapper;
import com.ashitoush.demo.repo.UsersRepo;
import com.ashitoush.demo.service.UserService;
import com.ashitoush.demo.util.CustomMessageSource;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UsersRepo usersRepo;
    private final UsersMapper usersMapper;
    private final CustomMessageSource customMessageSource;

    @Override
    public Integer saveUser(UserRequestDto requestDto) {
        Users users = Users.builder()
                .fullName(requestDto.getFullName())
                .contactNo(requestDto.getContactNo())
                .address(requestDto.getAddress())
                .build();
        return usersRepo.save(users).getId();
    }

    @Override
    public void updateUser(UserRequestDto requestDto) {
        Users users = usersRepo.findById(requestDto.getId())
                .orElseThrow(() -> new AppException(customMessageSource.get(MessageConstant.NOT_FOUND,
                        customMessageSource.get(FieldConstant.USER))));

        users.setFullName(requestDto.getFullName());
        users.setAddress(requestDto.getAddress());
        users.setContactNo(requestDto.getContactNo());
        usersRepo.save(users);
    }

    @Override
    public UserResponseDto getUserById(Integer id) {
        UserResponseDto userResponseDto = usersMapper.getUserById(id);
        if (userResponseDto == null) {
            throw new AppException(customMessageSource.get(MessageConstant.NOT_FOUND,
                    customMessageSource.get(FieldConstant.USER)));
        }
        return userResponseDto;
    }

    @Override
    public List<UserResponseDto> getAllUsers() {
        List<UserResponseDto> userResponseDtoList = usersMapper.getAllUser();
        if (userResponseDtoList.isEmpty()) {
            throw new AppException(customMessageSource.get(MessageConstant.NOT_FOUND,
                    customMessageSource.get(FieldConstant.USER)));
        }
        return userResponseDtoList;
    }

    @Override
    public void deleteUser(Integer id) {
        Users users = usersRepo.findById(id)
                .orElseThrow(() -> new AppException(customMessageSource.get(MessageConstant.NOT_FOUND,
                        customMessageSource.get(FieldConstant.USER))));
        usersRepo.delete(users);
    }
}
