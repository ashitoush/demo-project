package com.ashitoush.demo.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class BinarySearchAlgorithm {
    static int steps = 0;

    public int search(List<Integer> arrayList, int value) {
        int size = arrayList.size();
        int half = findHalfOfArray(size) - 1;
        if (half >= 0) {
            if (arrayList.get(half) == value) {
                steps++;
                log.info("Value {} found at index {} after {} steps", value, half, steps);
                return half;
            } else if (arrayList.get(half) <= value) {
                steps++;
                return search(arrayList.subList(half, size), value);
            } else {
                steps++;
                return search(arrayList.subList(0, half), value);
            }
        }
        log.info("Value not found after {} steps", steps);
        return -1;
    }

    public int findHalfOfArray(int size) {
        return (size/2);
    }

}
