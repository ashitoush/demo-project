package com.ashitoush.demo.constants;

public class MessageConstant {

    public static final String ERROR = "error";
    public static final String NOT_FOUND = "not.found";
    public static final String SAVE_SUCCESS = "save.success";
    public static final String UPDATE_SUCCESS = "update.success";
    public static final String RETRIEVE_SUCCESS = "retrieve.success";
    public static final String DELETE_SUCCESS = "delete.success";
    public static final String UNIQUE_CONSTRAINT_ERROR = "unique.constraint.error";
    public static final String BINARY_SEARCH_SUCCESS = "binary.search.success";
}
