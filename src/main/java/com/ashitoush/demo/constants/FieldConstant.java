package com.ashitoush.demo.constants;

public class FieldConstant {
    public static final String USER = "user";
    public static final String ORDER = "order";
    public static final String PRODUCT = "product";
}
