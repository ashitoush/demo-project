package com.ashitoush.demo.mapper;

import com.ashitoush.demo.dto.user.UserResponseDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UsersMapper {
    @Select("select *\n" +
            "from users u\n" +
            "where u.id = #{id}")
    UserResponseDto getUserById(Integer id);

    @Select("select *\n" +
            "from users")
    List<UserResponseDto> getAllUser();
}
