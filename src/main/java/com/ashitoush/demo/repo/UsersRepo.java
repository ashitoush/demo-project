package com.ashitoush.demo.repo;

import com.ashitoush.demo.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepo extends JpaRepository<Users, Integer> {
}
