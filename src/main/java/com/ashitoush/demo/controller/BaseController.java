package com.ashitoush.demo.controller;

import com.ashitoush.demo.dto.GlobalApiResponse;
import com.ashitoush.demo.util.CustomMessageSource;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

@RequiredArgsConstructor
public class BaseController {

    @Autowired
    protected CustomMessageSource customMessageSource;

//    Success API Status
    protected final boolean SUCCESS_API_STATUS = true;

//    Error API Status
    protected final boolean ERROR_API_STATUS = false;

    protected GlobalApiResponse successResponse(String message) {
        return GlobalApiResponse.builder()
                .status(SUCCESS_API_STATUS)
                .message(message)
                .data(null)
                .build();
    }

    protected GlobalApiResponse successResponse(String message, Object object) {
        return GlobalApiResponse.builder()
                .status(SUCCESS_API_STATUS)
                .message(message)
                .data(object)
                .build();
    }
}
