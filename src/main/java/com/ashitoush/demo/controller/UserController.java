package com.ashitoush.demo.controller;

import com.ashitoush.demo.constants.FieldConstant;
import com.ashitoush.demo.constants.MessageConstant;
import com.ashitoush.demo.dto.GlobalApiResponse;
import com.ashitoush.demo.dto.user.UserRequestDto;
import com.ashitoush.demo.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController extends BaseController {

    private final UserService userService;

    @PostMapping
    public ResponseEntity<GlobalApiResponse> save(@RequestBody @Valid UserRequestDto requestDto) {
        return ResponseEntity.ok(successResponse(customMessageSource.get(MessageConstant.SAVE_SUCCESS,
                customMessageSource.get(FieldConstant.USER)), userService.saveUser(requestDto)));
    }

    @PutMapping
    public ResponseEntity<GlobalApiResponse> update(@RequestBody @Valid UserRequestDto requestDto) {
        userService.updateUser(requestDto);
        return ResponseEntity.ok(successResponse(customMessageSource.get(MessageConstant.UPDATE_SUCCESS,
                customMessageSource.get(FieldConstant.USER))));
    }

    @GetMapping("/{id}")
    public ResponseEntity<GlobalApiResponse> getById(@PathVariable Integer id) {
        return ResponseEntity.ok(successResponse(customMessageSource.get(MessageConstant.RETRIEVE_SUCCESS,
                customMessageSource.get(FieldConstant.USER)), userService.getUserById(id)));
    }

    @GetMapping
    public ResponseEntity<GlobalApiResponse> getAllUsers() {
        return ResponseEntity.ok(successResponse(customMessageSource.get(MessageConstant.RETRIEVE_SUCCESS,
                customMessageSource.get(FieldConstant.USER)), userService.getAllUsers()));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<GlobalApiResponse> deleteUserById(@PathVariable Integer id) {
        userService.deleteUser(id);
        return ResponseEntity.ok(successResponse(customMessageSource.get(MessageConstant.DELETE_SUCCESS,
                customMessageSource.get(FieldConstant.USER))));
    }
}
