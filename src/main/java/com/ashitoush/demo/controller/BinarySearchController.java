package com.ashitoush.demo.controller;

import com.ashitoush.demo.constants.MessageConstant;
import com.ashitoush.demo.dto.GlobalApiResponse;
import com.ashitoush.demo.util.BinarySearchAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/binary-search")
@RequiredArgsConstructor
public class BinarySearchController extends BaseController {

    private final BinarySearchAlgorithm binarySearchAlgorithm;

    @PostMapping("/search/{value}")
    public ResponseEntity<GlobalApiResponse> getBinarySearchResult(@RequestBody List<Integer> arrayList, @PathVariable("value") Integer value) {
        return ResponseEntity.ok(successResponse(customMessageSource.get(MessageConstant.BINARY_SEARCH_SUCCESS), binarySearchAlgorithm.search(arrayList, value)));
    }
}
